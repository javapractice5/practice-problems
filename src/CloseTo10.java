//https://codingbat.com/prob/p172021

public class CloseTo10 {
    public int close10(int a, int b) {
        int aD = Math.abs(a - 10);
        int bD = Math.abs(b - 10);
        if (aD<bD)
            return a;
        if (bD<aD)
            return b;
        return 0;
    }
}
