public class ArmstrongNumber {
    static String armstrongNumber(int n)
    {
        int temp=n;
        int sum=0;
        while(n>0)
        {
            int rem=n%10;
            sum+=(rem*rem*rem);
            n=n/10;
        }
        if(temp==sum)
        {
            return "Yes";
        }
        else
            return "No";
        // code here
    }
}
